
const data = {};

export default {
  setVar: (db, key, val) => {
    if (!data[db]) data[db] = {};
    
    data[db][key] = val;
  },

  // Push a value to an array
  pushVal: (db, key, val) => {
    if (!data[db]) data[db] = {};
    if (!data[db][key]) data[db][key] = [];

    data[db][key].push(val);
  },

  getVar: (db, key) => {
    if (!data[db]) data[db] = {};

    return data[db][key];
  },
};
