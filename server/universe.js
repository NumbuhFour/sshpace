import mongoose from 'mongoose';
import { Celestial, User, Drone } from './models';
import chatManager from './chat_manager';

const mongoAuth = require('../mongo.auth');
mongoAuth.server = mongoAuth.replset = { socketOptions: { keepAlive: 120 } };
mongoose.connect('mongodb://localhost/sshpace_pretest', mongoAuth);

process.on('SIGINT', () => {
  console.log("SHUTDOWN TRIGGERED. CLOSING DB CONNECTION...");

  const saves = [];
  let client;
  for (let i in playerSessions) {
    client = playerSessions[i];
    saves.push(new Promise( (res, rej) => {
      client.user.online = false;
      client.user.save( () => {
        res();
      });
    }));
  }

  Promise.all(saves).then( () => {
    // @TODO track saves, only close when all finished
    mongoose.connection.close(() => {
      console.log('DB Connection closed.');
      process.exit(0);
    });
  });
});


const playerSessions = {};

const buildSectors = (sectors) => {
  Celestial.find({}, (err, list) => {
    list.forEach(c => { sectors[c._id] = c; });
  });
};


const Universe = class Unviverse {
  constructor() {
    this.sectors = {};
    buildSectors(this.sectors);
  }

  /************** FUN SHIT ************************/
  getOnlinePlayers() {
    return Object.keys(playerSessions);
  }

  getSector(sectorID) {
    return this.sectors[sectorID];
  }

  /********************** PLAYER LOGIN AND REGISTRATION ****************************/
  loginPlayer(client) {
    const name = client.name;
    User.find({ username: name }, (err, users) => {
      if (err) throw err;
      const user = users[0];
      if (!user) {
        console.error("Attempted to login user, but not found!", name);
      } 
      else {
        client.user = user;
        user.online = true;
        user.last_online = new Date();
        user.save((err) => {
          if(err) throw err;
        });
      }
    });
    playerSessions[name] = client;
  }

  logoutPlayer(client) {
    const name = client.name;
    if (!name) return;
    User.find({ username: name }, (err, users) => {
      if (err) throw err;
      const user = users[0];
      if (!user) {
        console.err("Attempted to logout user, but not found!", name);
      } 
      else {
        user.online = false;
        user.last_online = new Date();
        user.save((err) => {
          if(err) throw err;
        });
      }
    });
    delete playerSessions[name];
  }


  checkPassword(username, pass, callback) {
    const playerList = User.find({ username: username.toLowerCase()}, (err, users) => {
      if (err) throw err;
      const user = users[0];
      if (!user) callback (false);
      else if (user.pass_hash !== pass) callback(false);
      else callback(true); // @TODO hash pass
    });
  }

  /*
   * Check a username's validity
   Error codes:
    0   = no error
    1   = already logged
    10  = not alphanumeric
    11  = too short
   */
  validateUsernameLogin(name, callback) {
    // Check if username is logged in
    // Check if user does not exist
    // Check if username is valid

    const playerList = User.find({username: name.toLowerCase()}, (err, users) => {
      if (err) throw err;
      const user = users[0];
      console.log("USERCHECK ", user);
      if (!user) { // User not found, now check validity
        if(name.trim().length < 3) { // min 3 characters
          callback({ err: 11, msg: "Usernames must be at least 3 characters long." });
        }
        else if (/^[a-zA-Z0-9]+$/.test(name)) { // Is alphanumeric
          callback({ err: 0, exists: false });
        }
        else {
          callback({ err: 10, msg: "Usernames must be strictly alphanumeric." });
        }
      }
      else { // Are they logged in already?
        if (playerSessions[name.toLowerCase()]) callback({ err: 1, msg: "User already logged in." });
        else callback({ err: 0, exists: true });  
      }
    });
  }

  validateEmailRegistration(email, callback) {
    // Check if email does not exist
    // Check if email is valid

    const playerList = User.find({email: email.toLowerCase()}, (err, users) => {
      if (err) throw err;
      const user = users[0];
      const validReg = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      if (!user) { // User not found, now check validity
        if(!validReg.test(email.toLowerCase().trim())) callback({ err: 10, msg: 'Email invalid.' });
        else callback({ err: 0 });
      }
      else {
        callback({ err: 1, msg: 'Email already registered.' });
      }
    });
  }

  createUser(nick, email, pass, onComplete) {
    nick = nick.trim();
    const newUser = new User({
      username: nick.toLowerCase(),
      display_name: nick,
      email,
      pass_hash: pass,
    });

    const newDrone = new Drone({
      name: "Alpha Drony",
      location: this.sectors[Object.keys(this.sectors)[0]]._id,
      owner: newUser._id,
    });

    newUser.drones.push(newDrone._id);

    const usave = new Promise( (res, rej) => {
      newUser.save(err => {
        if(err) throw err;

        res();
      });
    });

    const dsave = new Promise( (res, rej) => {
      newDrone.save(err => {
        if(err) throw err;

        res();
      });
    });

    Promise.all( [usave, dsave] ).then( () => {
        console.log("New user registered: " + nick);
        if(onComplete) onComplete();
    });
  }
}

const universe = new Universe();

export default universe;

