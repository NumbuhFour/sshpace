import mongoose from 'mongoose';

export const schema = new mongoose.Schema({
  name: String,

  description: String,

  slot: { type: Number, required: true },

  created_at: Date,
  updated_at: Date,
});

schema.pre('save', function(next) {
  const currentDate = new Date();
  this.updated_at = currentDate;
  if (!this.created_at)
    this.created_at = currentDate;
  next();
})

const model = mongoose.model('DroneComponent', schema);
export default model;




