
import mongoose from 'mongoose';

export const schema = new mongoose.Schema({
  name: String,

  location: String,

  owner: String,

  created_at: Date,
  updated_at: Date,
});

schema.pre('save', function(next) {
  const currentDate = new Date();
  this.updated_at = currentDate;
  if (!this.created_at)
    this.created_at = currentDate;
  next();
})
export default mongoose.model('Drone', schema);


