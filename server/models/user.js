
import mongoose from 'mongoose';

// @TODO
// UN Validation
// Permissions system

export const schema = new mongoose.Schema({
  username: { type: String, lowercase: true, trim: true, required: true, unique: true },
  display_name: { type: String, trim: true, required: true }, // Version with caps
  email: { type: String, lowercase: true, trim: true, required: true, unique: true },
  pass_hash: { type: String, required: true },
  
  drones: [ String ],

  online: Boolean,
  last_online: Date,

  created_at: Date,
  updated_at: Date,
});

schema.pre('save', function(next) {
  const currentDate = new Date();
  this.updated_at = currentDate;
  if (!this.created_at)
    this.created_at = currentDate;
  next();
});

export default mongoose.model('User', schema);


