import mongoose from 'mongoose';

const neighborSchema = new mongoose.Schema({
  target_id: String,
  distance: Number,
});

export const schema = new mongoose.Schema({
  name: { type: String, required: true, unique: true },

  description: String,

  type: { type: Number, required: true },
  neighbors: [neighborSchema],

  created_at: Date,
  updated_at: Date,
});

schema.pre('save', function(next) {
  const currentDate = new Date();
  this.updated_at = currentDate;
  if (!this.created_at)
    this.created_at = currentDate;
  next();
})

const model = mongoose.model('Celestial', schema);
export default model;




