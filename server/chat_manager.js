import universe from './universe';

const channels = {};
const openClients = {};

const COLORS = {
  PRIVATE: '{purple-fg}', // Privatge messages
  CHANNEL_TALK: '{cyan-fg}', // People talking
  CHANNEL_MSG: '{#FFA500-fg}', // Message from channel
  SELF: '{gray-bg}{blue-fg}', // Message from self
  TEXT: '{white-fg}', // Text contents
};

class Channel {
  constructor(name, sector, isstatic) {
    this.name = name;
    this.sector = sector;
    this.isstatic = isstatic;
    this.clients = [];

    this.specialCommands = {
      help: this.help,
      quit: this.quit,
      list: this.list,
    };
  }

  addClient(client) {
    this.postRawMessage(COLORS.CHANNEL_MSG + client.name + ' has connected.{/}');
    this.clients.push(client);
  }

  removeClient(client) {
    this.clients.splice(this.clients.indexOf(client), 1);
    if (!this.isstatic && this.clients.length <= 0) {
      def.removeChannel(this);
    }
    else {
      this.postRawMessage(COLORS.CHANNEL_MSG + client.name + ' has disconnected.{/}');
    }
  }

  postRawMessage(message) {
    message = message.trim();
    if (message.length == 0) return;

    this.clients.forEach((client) => {
      client.chat(message);
    });
  }
  postMessage(sender, message) {
    message = message.trim();
    if (message.length == 0) return;

    // Handle special commands
    if (message.startsWith('/')) {
      const split = message.split(' ');
      const cmd = message.substring(1);
      if (this.specialCommands[cmd])
        this.specialCommands[cmd](sender, message);
      else {
        sender.chat(CHANNEL_MSG + ' command not found: ' + cmd + '{/}');
      }
    }

    // Send actual message
    else {
      const msg = COLORS.CHANNEL_TALK + sender.name + ': ' + COLORS.TEXT + message + '{/}';
      this.clients.forEach((client) => {
        if (client == sender)
          client.chat(COLORS.SELF + sender.name + ': ' + COLORS.TEXT + message + '{/}');
        else
          client.chat(msg);
      });
    }
  }

  // Special commands
  help(sender, message) {
    sender.chat(COLORS.CHANNEL_MSG + 'Commands: {/}');
    for(let cmd in this.specialCommands) {
      sender.chat('  ' + COLORS.CHANNEL_MSG + cmd + '{/}');
    }
  }

  quit(sender, message) {
    def.exitChannel(sender);
  }

  list(sender, message) {
    sender.chat(COLORS.CHANNEL_MSG + 'Connected clients: {/}');
    for(let client in this.clients) {
      const name = this.clients[client].name
      sender.chat('  ' + COLORS.CHANNEL_MSG + name + '{/}');
    }
  }
}

const def = {

  getChannels: () => {
    return Object.keys(channels);
  },

  enterChannel: (client, channel) => {
    if (!channel.startsWith('#')) {
      client.log('Channel not found.');
      return 1;
    }
    if (client.channel) def.exitChannel(client);

    client.clearChat();
    client.chat(COLORS.CHANNEL_MSG + 'Connected to ' + channel + '{/}');
    if (channels[channel]) {
      channels[channel].addClient(client);
      client.channel = channels[channel];
    }
    else {
      const ch = channels[channel] = new Channel(channel, undefined, false);
      ch.addClient(client);
      ch.postRawMessage(COLORS.CHANNEL_MSG + 'This channel is newly created.{/}');
      client.channel = ch;
    }
    return 0;
  },

  exitChannel: (client) => {
    if (!client.channel) return 0;
    const ch = channels[client.channel.name];
    if (!ch) return 1;
    ch.removeClient(client);
    client.clearChat();
    client.chat(COLORS.CHANNEL_MSG + 'Disconnected.');
    client.channel = undefined;
    return 0;
  },

  // Send a message to a channel
  // @param targetChannel either a #channel or a username
  // @param message the message
  sendMessage: (sender, targetChannel, message) => {
    // Posts message to all users in channel
    // Or sends message directly to comms-open player
    
    if (targetChannel.startsWith('#')) {
      if (channels[targetChannel]) {
        channels[targetChannel].postMessage(sender, message);
        return 0;
      }else {
        client.log ('Channel not found.');
        return 1;
      }
    }
    else {
      const target = openClients[targetChannel];
      if (!target) {
        sender.log('Target\'s communications receiver not found : ' + targetChannel);
        return 1;
      }
      else {
        const msg = COLORS.PRIVATE + sender + ': ' + message + '{/}';
        sender.chat(msg);
        target.chat(msg);
        return 0;
      }
    }

  },

  openComms: (client, channel) => {
    client.openChat();
    openClients[client.name] = client;
    if (channel) {
      def.enterChannel(client, channel);
    }
    return 0;
  },
  closeComms: (client) => {
    if (client.channel)
      def.exitChannel(client, client.channel.name);

    client.closeChat();

    delete openClients[client.name];
    return 0;
  },

  createStaticChannel:(channel, sector) => {
    if(sector) {
      channels[sector.name + '#' + channel] = new Channel(channel, sector, true);
    }
    else {
      channels['#' + channel] = new Channel(channel, undefined, true);
    }
  },

  // For removing a channel when its empty
  removeChannel: (channel) => {
    delete channels[channel.name];
  },
};

export default def;


def.createStaticChannel('global');
