import mongoose from 'mongoose';
import models from './server/models';
import constants, { CELESTIALS } from './constants'

console.log("C", constants, CELESTIALS);
// This is for populating static pieces of the DB

const mongoAuth = require('./mongo.auth');
mongoAuth.server = mongoAuth.replset = { socketOptions: { keepAlive: 120 } };
mongoose.connect('mongodb://localhost/sshpace_pretest', mongoAuth);

const close = () => {
  // @TODO track saves, only close when all finished
  mongoose.connection.close(() => {
    console.log('DB Connection closed.');
    process.exit(0);
  });
};

process.on('SIGINT', () => {
  console.log("SHUTDOWN TRIGGERED. CLOSING DB CONNECTION...");
  close();
});


let counter = 2;
const done = () => {
  console.log("COUNT", counter);
  counter --;
  if (counter == 0) {
    models.Celestial.findById(testStation.id, (err, find) => {
      console.log ("FOUND " + find);
      if(err) throw err;
      find.neighbors = [
        {
          target_id: testPlanet.id,
          distance: 0,
        }
      ];

      find.save((err) => {
        if(err)throw err; 
        console.log("SAVE station");
      });
    });

    models.Celestial.findById(testPlanet.id, (err, find) => {
      console.log("FOUND2 " + find);
      if(err) throw err;
      find.neighbors = [
        {
          target_id: testStation.id,
          distance: 0,
        }
      ];

      find.save((err) => {
        if(err)throw err; 
        console.log("SAVE planet");
      });
    });   
  }
}

const celestials = [
  {
    name: 'Test Station Alpha',
    type: constants.CELESTIALS.STATION,
    description: 'First of many stations to inhabit this universe of ours. voop',
    neighbors: [],
  },
  {
    name: 'Earth',
    type: constants.CELESTIALS.PLANET,
    description: 'A tiny blue dot speckled with dirt and glory.',
    neighbors: [['Test Station Alpha', 0], ['Luna', 0] ],
  },
  {
    name: 'Luna',
    type: constants.CELESTIALS.MOON,
    description: 'Earth\'s moon',
    neighbors: [['Rubbage', 0]],
  },

  {
    name: 'Rubbage',
    type: constants.CELESTIALS.ASTEROID_FIELD,
    description: 'An asteroid field of debris.',
    neighbors: [],
  },

];

// Link all neighboring celestials together
celestials.forEach(cel => {
  cel.neighbors.forEach(neighborEntry => {
    
    // Find the other celestials entry we're linking to
    const toLink = celestials.filter(check => check.name == neighborEntry[0])[0];
  
    // Don't add ourselves if it already exists
    if (toLink.neighbors.filter(check => check[0] == cel.name).length > 0) return;

    // Add ourselves to the other celestial's list of neighbors
    toLink.neighbors.push([cel.name, neighborEntry[1]]);
  });
});


const shortcutIndicies = {};
const celestialModels = []

const makeProms = [];
for(let i in celestials) {
  const cel = celestials[i];
  shortcutIndicies[cel.name] = cel;

  // Make promises which create/update document
  makeProms.push(new Promise(function(i, res, rej) {
    models.Celestial.find({ name: cel.name }, function(i, err, finds) {
      if (err) throw err;

      const cel = celestials[i];
      if (finds.length > 0) {
        console.log('Updating celestial...', finds);
        const find = finds[0];
        find.name = cel.name;
        find.type = cel.type;
        find.description = cel.description;
        find.neighbors = [];
        find.save(function(i, err) {
          if(err) throw err;
          celestials[i].id = find._id;
          shortcutIndicies[cel.name] = celestials[i];
          res([find, i]);
        }.bind(null,i));
      }
      else {
        const clone = JSON.parse(JSON.stringify(cel));
        clone.neighbors = [];
        const n = new models.Celestial(clone);
        console.log("Creating new celestial");
        n.save((function(i, err) {
          if(err) throw err;
          console.log("New celestial created");
          celestials[i].id = n._id;
          shortcutIndices[cel.name] = celestials[i];
          res([n, i]);
        }.bind(null, i)));
      }
    }.bind(null, i));
  }.bind(null, i)));
}
console.log('Completed transfer setup. ', shortcutIndicies);
console.log('Celelstials: ', celestials);

Promise.all(makeProms).then(values => {
  console.log('Update done, starting linking...', celestials);
  const linkProms = [];

  values.forEach(val => {
    const neighborsInsert = [];
    const cel = celestials[val[1]];

    // Build the neighbors list with the updated ids
    cel.neighbors.forEach(neighborEntry => {
      neighborsInsert.push({
        target_id: shortcutIndicies[neighborEntry[0]].id,
        distance: neighborEntry[1],
      });
    });

    val[0].neighbors = neighborsInsert;

    // Make the promise for saving
    linkProms.push(new Promise((res, rej) => {
      
      val[0].save((err) => {
        if(err) {
          console.log('Celestial final save error');
          rej();
          throw err;
        }

        res();
      });

    }));

  });

  Promise.all(linkProms).then( () => {
    console.log('Celestials complete.');
    close();
  });

});

/*
const testStation = new models.Celestial({
  name: 'Test Station Alpha',
  type: constants.CELESTIALS.STATION,
  description: 'First of many stations to inhabit this universe of ours.',
  neighbors: [],
});
testStation.save((err, o) => { 
  if(err) throw err;
  testStation.id = o.id;
  done();
} );

const testPlanet = new models.Celestial({
  name: 'Earth',
  type: constants.CELESTIALS.PLANET,
  description: 'A tiny blue dot speckled with dirt and glory.',
  neighbors: [],
});
testPlanet.save((err, o) => {
  if(err) throw err;
  testPlanet.id = o.id;
  done();
} );


*/
