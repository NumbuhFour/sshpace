export default {
  MANIPUlATOR: 0,
  ENGINE: 1,
  HULL: 2,
  THRUSTER: 3,
  FUEL_STORAGE: 4,
  CARGO_BAY: 5,
  SHIELD: 6,
  BOOSTER: 10,
}
