export default {
  STATION: 0,
  PLANET: 1,
  ASTEROID_FIELD: 2,
  MOON: 3,
};
