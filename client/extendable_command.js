
export default class ExtendableCommand {
  constructor (client, resolve) {
    this.client = client;
    this.resolve = resolve;
    this.init();
  }

  setCommandHandlerStack(cmdList) {
    this.commandHandlerStack = cmdList;
  }

  init() {

  }

  /*
  Handles input from user

  @param line String Full line user entered
  @param params [String] List of space-separated params
  @param pipe String the piped input from the previous command (if available)
  */
  handleInput(line, params, pipe) {
    this.resolve(0);
  }
}

export const key = 'err';
