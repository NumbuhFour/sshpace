
import fs from 'fs';
import blessed from 'blessed';
import ssh2, { Server } from 'ssh2';

import InputHandler from './input_handler';

import universe from '../server/universe';
import chatman from '../server/chat_manager';

import Client from './Client.js';

const cliHan = {

  connections: [],

  server : new Server({
      hostKeys: [fs.readFileSync('host.key')],
    }, (client) => {
      
      let stream;
      let checkPasswordState = 0;
      client.pwAttempts = 0;

      client.on('authentication', (ctx) => {

        let nick = client.name ? client.name : ctx.username;
        let pass = ctx.password;

        if (ctx.method == 'password') {

          // Checking password for login
          if (checkPasswordState == 1) {
            // Request password
            universe.checkPassword(nick, pass, (success) => {

              if (!success) {
                if (client.pwAttempts >= 3) {
                  ctx.reject();
                }
                else {
                  client.pwAttempts ++;
                  setTimeout(() => {
                    ctx.reject(['password']);
                  }, client.pwAttempts * 1000);
                }
                //ctx.prompt('Wrong password. Attempts remaining: ' + attempts + '\nEnter Password: ', loginPromptCP.bind(null, attempts-1));
              }
              else {
                console.log("Successful!!");
                client.name = nick;
                universe.loginPlayer(client);
                ctx.accept();
              }
            });
          }
          else if (checkPasswordState == 2) {
            // Request password @TODO password validation
              // Create user
            universe.createUser(nick, client.email, pass, () => {
              delete client.email;
              client.name = nick;
              universe.loginPlayer(client);
              ctx.accept();
            });
          }
          return;
        }
        // Force into interactive mode
        if (ctx.method !== 'keyboard-interactive')
          return ctx.reject(['keyboard-interactive'])

        // Attempt to login
        // Request password
        
        const usernamePromptCP = (answers) => {
          universe.validateUsernameLogin(answers[0], (rtn) => {
            if (rtn.err > 0) { 
              ctx.prompt('Error with username: ' + rtn.msg + '\nDo better: ', usernamePromptCP);
            }
            else {
              client.name = nick;
              // Username found, attemmpt logging in
              if(rtn.exists) {
                checkPasswordState = 1;
                ctx.reject(['password']);
              }
              else {
                let email;
                let pass;
                // Create new user
                const emailPromptCP = (answers) => {
                  universe.validateEmailRegistration(answers[0], (rtn) => {
                    if (rtn.err > 0) {
                      ctx.prompt('Error with email: ' + rtn.msg + '\nDo better: ', emailPromptCP);
                    }
                    else {
                      email = answers[0];
                      client.email = email;
                      checkPasswordState = 2;
                      ctx.reject(['password']);

                    }
                  });
                }

                ctx.prompt('That username is not registered. Continue to register as a new user: \n\nEnter an email: ', emailPromptCP);
              }
            }
            // User not found: request to create
            //  ask for email
            //  ask for password
            //  create user
          });
        };
        usernamePromptCP([nick]);

        return;

        // Name validation check TODO
        let invalid = cliHan.isNameInvalid(nick);
        if (!invalid) { 
          client.name = nick.toLowerCase();;
          return ctx.accept(); 
        } 
        else {
          if (ctx.method !== 'keyboard-interactive')
            return ctx.reject(['keyboard-interactive']);

          ctx.prompt(invalid, function retryPrompt(answers) {
            if (answers.length === 0)
              return ctx.reject(['keyboard-interactive']);

            nick = answers[0];

            invalid = cliHan.isNameInvalid(nick);
            if (!invalid) {
              client.name = nick.toLowerCase();
            }
            else {
              return ctx.prompt(invalid, retryPrompt);
            }
          });

          return ctx.accept();
        }
      })



      .on('ready', () => {
        let rows;
        let cols;
        let term;

        client.once('session', (accept, reject) => {
          accept()

          // First time connect: Receive window size
          .once('pty', (acc, rej, info) => {
            rows = info.rows;
            cols = info.cols;
            term = info.term;
            acc && acc();
          })

          // Any time the user changes window sizes
          .on('window-change', (acc, rej, info) =>  {
            rows = info.rows;
            cols = info.cols;

            if (stream) {
              stream.rows = rows;
              stream.columns = cols;
              stream.emit('resize');
            }
            acc && acc();
          })
          
          .once('shell', (acc, rej) => {
            stream = acc();
            client.stream = stream;
            cliHan.connections.push(client);

            stream.name = client.name;
            stream.rows = rows || 24;
            stream.columns = cols || 80;
            stream.isTTY = true;
            stream.setRawMode = (v)=>{};
            stream.on('error', (err) => {
              console.log('Stream error:', err);
            });

            const screen = new blessed.screen({
              autoPadding: true,
              smartCSR: true,
              program: new blessed.program({
                input: stream,
                output:stream,
              }),
              terminal: term || 'ansi',
            });

            screen.title = 'Welcome to SSHPACE ' + client.name + '!';
            
            var clientInstance = new Client(client, client.name);

            const input = new InputHandler(screen, clientInstance);

            screen.render();
            // Recommended for some terminals
            screen.emit('resize');
        });


      })

      // User disconnect
      .on('end', () => {
        console.log("Player has disconnected " + client.name);
        universe.logoutPlayer(client); 
        chatman.closeComms(client);
        
      })
      
      .on('error', (err) => {
        console.log('Error with user: ' + err);
      });

    });

  }),


  // Check if name is valid and unused. Returns reason or false
  isNameInvalid: (name) => {
    if(name == "test") return "Name invalid!"; 
  },

  start: (port) => {
    cliHan.server.listen(port,function() {
      console.log('Listening on port ' + this.address().port);
    });
  },
};

module.exports = cliHan;
