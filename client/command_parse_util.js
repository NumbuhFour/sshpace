
// Split the command by pipes | avoiding those in quotations
export const parsePipes = (line) => {
  const split = [];
  let s = 0;
  let inDQ = false; // "
  let inSQ = false; // '
  let inBQ = false; // `
  let i = 0;
  for(i = 1; i < line.length; i++) {
    const c = line.charAt(i);
    const lastChar = line.charAt(i-1);
    if (lastChar != '\\') { //@TODO handle idiots who do \\\\\\\\\
      if (c == '"') inDQ = !inDQ;
      if (c == "'") inSQ = !inSQ;
      if (c == '`') inBQ = !inBQ;
    }
    if (line.charAt(i) == '|'  && !inDQ && !inSQ && !inBQ) {
      split.push(line.substring(s, i));
      s = i + 1;
    }
  }
  if (s < i) { // Add final line
    split.push(line.substring(s, i));
  }

  return split;
};

// Split the command by spaces avoiding those in quotations
export const splitParams = (line) => {
  const split = [];
  let s = 0;
  let inDQ = false; // "
  let inSQ = false; // '
  let inBQ = false; // `
  let i = 0;
  for(i = 1; i < line.length; i++) {
    const c = line.charAt(i);
    const lastChar = line.charAt(i-1);
    if (lastChar != '\\') { //@TODO handle idiots who do \\\\\\\\\
      if (c == '"') inDQ = !inDQ;
      if (c == "'") inSQ = !inSQ;
      if (c == '`') inBQ = !inBQ;
    }
    if (line.charAt(i) == ' '  && !inDQ && !inSQ && !inBQ) {
      split.push(line.substring(s, i));
      s = i + 1;
    }
  }
  if (s < i) { // Add final line
    split.push(line.substring(s, i));
  }

  return split;
};
