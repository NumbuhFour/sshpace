import blessed from 'blessed';
import Terminal from './terminal';

// States
const BASIC_TERMINAL = 0;


const onTerminalSubmit = function(input, output, line) {
  input.clearValue();
  output.add(line);
  this.screen.render();

  input.focus();
};

const InputHandler = function(screen, client) {
  this.screen = screen;
  this.client = client;
  const stream = client.stream;

  this.terminal = new Terminal(screen, client);

  screen.key('C-z,C-q', () => client.closeConnection());
};

module.exports = InputHandler;
