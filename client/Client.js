
export default class Client {
  constructor (client, name) {
    this._client = client;
    this._client.client = this;
    this._name = name;

    this._activeDrone = []; // For push/pop
  }

//// GET SET ////

  set terminal (term) {
    this._terminal = term;
  }
  get terminal () { return this._terminal; }

  get client () { return this._client; }


  get name () { return this._name; }

  get activeDrone () {
    if (this._activeDrone.length == 0) return undefined;
    return this._activeDrone[this._activeDrone.length - 1];
  }

  set activeDrone (val) {
    if (val == undefined) { 
      this._activeDrone.pop(); 
    }
    else this._activeDrone.push(val);
  }

//// FORWARD FUNCTS ////

  chat (msg) { this.terminal.chat(msg); }
  
  clearChat () { this.terminal.clearChat(); }

  log (msg) { this.terminal.log(msg); }


//// SYS ACTIONS ////

  closeConnection () {
    this._client.stream.end();
  }
  
//// USER ACTIONS ////

  // PWD?
  // Current active drone
  // Drone list
  // chat var shit
  // terminal var shit


};
