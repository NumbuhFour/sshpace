
import blessed from 'blessed';
import CommandHandler from './command_handler.js';
import { parsePipes, splitParams } from './command_parse_util'; 

export default class Terminal {
  constructor(screen, client, placement) {
    this._screen = screen;
    this._client = client;
    this._client.terminal = this;

    this._activeCmd = undefined;

    this._commandHandler = new CommandHandler(__dirname + '/cmd',
                                              this._client,
                                              this._client.closeConnection);

    // @TODO implement placement param
    this._container = new blessed.box({
      screen,
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
    });

    this.activate();
    this.setup();
    this._pwds = [];
    this.pushPwd("terminal");

    this._cmdBack = [];
    this._cmdBackIndex = -1;

    this.focus();

    this._cmdQueue = [];
  }

//// FORWARDED FUNCTIONS ////
  
  log (msg) {
    this._outputBox.add(msg);
    this._screen.render();
  }
  
  openchat () { this._chatBox.show(); }
  closeChat () { this._chatBox.hide(); }

  chat (msg) { this._chatBox.add(msg); }
  clearChat () { this._chatBox.setContent(''); }


//// SYS ACTIONS ////
  activate () {
    // chick if attached first
    this._screen.append(this._container);
    this._screen.render();
  }

  deactivate () {
    this._container.detach();
  }

  focus() {
    this._inputBox.focus();
  }

  setup() {
    // Main area of scrollback
    this._outputBox = new blessed.log({
      screen: this._screen,
      parent: this._container,
      top: 0,
      left: 0,
      bottom: 2,
      scrollOnInput: true,
      tags: true,
    });
    this._container.append(this._outputBox);

    // Second scrollback window for chat log
    this._chatBox = new blessed.log({
      screen: this._screen,
      parent: this._container,
      top: 0,
      right: 0,
      width: '30%',
      scrollOnInput: true,
      border: {
        type: 'line',
      },
      style: {
        border: {
          fg: '#f0f0f0',
        },
      },
      tags: true,
    });
    this._chatBox.hide();
    this._container.append(this._chatBox);

    // Box for showing current directory
    this._pwdBox = new blessed.box({
      screen: this._screen,
      parent: this._container,
      height: 1,
      bottom: 1,
      width: '100%',
    });
    this._container.append(this._pwdBox);

    // Separator
    this._container.append(new blessed.box({
      screen: this._screen,
      parent: this._container,
      height: 1,
      bottom: 2,
      left: 0,
      width: '100%',
      type: 'line',
      ch: '=',
    }));

    // User input box
    this._inputBox = new blessed.textbox({
      screen: this._screen,
      parent: this._container,
      bottom: 0,
      height: 1,
      width: '100%',
      inputOnFocus: true,
    });
    this._container.append(this._inputBox);

    //this._inputBox.focus();

    this._inputBox.on('submit', this.onSubmit.bind(this));

    this._inputBox.key('C-c', () => {
      this._client.closeConnection();
    });

    this._inputBox.key('up', () => {
      if (this._cmdBack.length == 0) return;

      this._cmdBackIndex ++;
      if (this._cmdBackIndex >= this._cmdBack.length) this._cmdBackIndex = this._cmdBack.length - 1;

      this._inputBox.setValue(this._cmdBack[this._cmdBack.length - 1 - this._cmdBackIndex]);
      this._screen.render();
    });
    this._inputBox.key('down', () => {
      if (this._cmdBack.length == 0) return;

      this._cmdBackIndex --;
      if (this._cmdBackIndex < -1) this._cmdBackIndex = -1;

      if (this._cmdBackIndex == -1) {
        this._inputBox.setValue('');
        this._screen.render();
      }
      else{
        this._inputBox.setValue(this._cmdBack[this._cmdBack.length - 1 - this._cmdBackIndex]);
        this._screen.render();
      }
    });

  }

  pushPwd(pwd) {
    this._pwd = pwd;
    this._pwds.push(pwd);
    this._pwdBox.setContent(this._client.name + '@' + pwd + '>');
    this._screen.render();
  }
  popPwd(){
    const rtn = this._pwds.pop();
    this._pwd = this._pwds[this._pwds.length - 1];
    this._pwdBox.setContent(this._client.name + '@' + this._pwd + '>');
    return rtn;
  }

  addCmdLine(line) {
    this._outputBox.add('\n{cyan-fg}' + this._client.name + '@' + this._pwd + '>{/} ' + line);
  }

  onSubmit(line) {
    if (line.trim().length == 0) {
      this.addCmdLine('');
      this._inputBox.clearValue();
      this._screen.render();
      this.focus();
      return;
    }

    this._cmdBack.push(line);
    if(this._cmdBack.length > 25) this._cmdBack.splice(1);
    this._cmdBackIndex = -1;

    this._inputBox.clearValue();
    if(!this._activeCmd) this.addCmdLine(line);
    this._screen.render();
    
    this._commandHandler.onSubmit(line.trim());

    this.focus();
  } 
}
