import universe from '../server/universe';

export default class Drone {
  
  constructor(droneModel) {
    this.model = droneModel;
    
    this.sector = universe.getSector(this.model.location);
  }

  getSector() { return this.sector; }
  getName() { return this.model.name; }

  getNeighbors() {
    return this.sector.neighbors.map(s => universe.getSector(s.target_id));
  }

  findNeighborByName(name) {
    const neighbors = this.getNeighbors();
    return neighbors.filter(n => n.name === name)[0];
  }

  travelTo(sector) {
    if (!sector || !sector._id) {
      return false;
    }
    this.model.location = sector._id;
    this.model.save((err) => {
      if (err) throw err;
    });
    this.sector = sector;
    return true;
  }

  getStatus() {
    return {
      name: this.model.name,
      location: this.sector.name,
    };
  }

};
