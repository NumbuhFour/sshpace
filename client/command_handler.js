import fs from 'fs';
import path from 'path';
import { parsePipes, splitParams } from './command_parse_util'; 

const commandsByDir = {};

export default class CommandHandler {
  
  constructor (root, client, closeCall, extraParams, parentCmd) {
    // @todo alias
    this.client = client;
    this.root = root;
    this.closeCall = closeCall; // What to call when closing
    this.parentCmd = parentCmd;
    this.extraParams = extraParams;
    if (!commandsByDir[root]) {
      commandsByDir[root] = {};
      this.buildCommandList(root);
    }
    this.test = 0;
  }

  getCommandList () {
    return commandsByDir[this.root];
  }

  buildCommandList (dir) {
    fs.readdirSync(dir).forEach(file => {
      if (!file.endsWith('.js')) return;
      const cmd = require(path.join(dir, file));
      commandsByDir[this.root][cmd.key] = cmd.default;
    })
  }

  findCommand(line, childCallerStack) {
    childCallerStack.push(this);
    let rtn = this.handleCommand(line, childCallerStack);
    if (!rtn && this.parentCmd) {
      rtn = this.parentCmd.findCommand(line, childCallerStack);
    }

    return rtn;
  }

  onSubmit(line) {

    if (!this.activeCmd) {
      const split = parsePipes(line);

      // @TODO handle up in command stack

      this.cmdQueue = split;

      let rtn = this.handleCommand(split[0]);
      if (!rtn && this.parentCmd)
        rtn = this.parentCmd.findCommand(split[0], [ this ]);


      if (!rtn) {
        this.client.log('Command not found: ' + split[0].split(' ')[0]);
      }
      else {
        this.activeCmd = rtn;
        rtn.handleInput(split[0], splitParams(split[0]));
      }

      this.cmdQueue.shift(); // Remove first
    }
    else {
      this.activeCmd.handleInput(line, splitParams(line));
    }
  }

  handleCommand(line, childCallerStack) {
    childCallerStack = childCallerStack || [this];
    const cmd = commandsByDir[this.root][line.split(' ')[0]];

    if (cmd != undefined) {
      let inst;
      if (!this.extraParams)
        inst = new cmd(this.client, childCallerStack[0].completeCommand.bind(childCallerStack[0]));
      else {
        const e = 'new cmd(this.client, this.completeCommand.bind(this), ' + this.extraParams.map((o,i) => ('this.extraParams[' + i + ']')).join(',') + ')';
        console.log(e);
        inst = eval(e);
      }
      inst.parentCommandHandler = this;

      inst.setCommandHandlerStack(childCallerStack);
      return inst;
    }

    return undefined;
  }

  completeCommand(status, output) {
    this.activeCmd = undefined;
    if (status != 0) {
      this.client.log("Program returned error code: " + status);
    }
    else {
      if (this.cmdQueue.length > 0) {
        const next = this.cmdQueue[0].trim();
        this.cmdQueue.shift();
        this.activeCmd = this.handleCommand(next);
        if (!this.activeCmd && this.parentCmd)
          this.activeCmd = this.parentCmd.findCommand(next, [ this ]);

        if (!this.activeCmd) {
          this.client.log('Command not found: ' + next.split(' ')[0]);
        }
        else {
          this.activeCmd.handleInput(next, splitParams(next), output);
        }
      }
      else {
        if (output) {
          if (Array.isArray(output)) {
            output.forEach( o => this.client.log(o));
          }
          else {
            this.client.log((typeof output === 'string' || output instanceof String) ? output : JSON.stringify(output));
          }
        }
      }
    }
  }
  

};
