
import Command from '../extendable_command';

export default class Exit extends Command {

  handleInput(line, params, pipe) {
    if (this.commandHandlerStack)
      this.commandHandlerStack[0].closeCall();
    else
      this.parentCommandHandler.closeCall();
    this.resolve(0);
  }
}

export const key = 'exit';
