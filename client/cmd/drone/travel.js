
import Command from '../../drone_command';

export default class Travel extends Command {

  handleInput(line, params, pipe) {
    const name = params.slice(1).join(' ');
    
    if (this.drone.travelTo(this.drone.findNeighborByName(name))){
      this.resolve(0);
    }
    else {
      this.resolve(1);
    }
  }
}

export const key = 'travel';
