
import Command from '../../drone_command';
import universe from '../../../server/universe';

export default class Status extends Command {

  handleInput(line, params, pipe) {
    this.resolve(0, this.drone.getStatus());
  }
}

export const key = 'status';
