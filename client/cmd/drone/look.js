
import Command from '../../drone_command';
import universe from '../../../server/universe';

export default class Look extends Command {

  handleInput(line, params, pipe) {
    this.resolve(0, this.drone.getNeighbors().map(n => n.name));
  }
}

export const key = 'look';
