
import Command from '../../drone_command';
import { Drone, User } from '../../../server/models';

export default class Scan extends Command {

  handleInput(line, params, pipe) {
    Drone.find({ location : this.drone.getSector()._id }, (err, found) => {
      if (err) throw err;

      if (found.length == 0) {
        this.resolve(0, 'No drones found wut.');
      }
      else {
        const proms = [];
        found.forEach(o => {
          proms.push(new Promise((res, rej) => {
            User.findById(o.owner, (err, user) =>{
              if (err) throw err;
              res([o, user]);
            });
          }));
        });

        Promise.all(proms).then((values) => {
          const rtn = values.map(val => val[0].name + ' ' + val[1].display_name);
          console.log('RTN: ' + rtn);
          this.resolve(0, rtn);
        });
      }
    });
  }
}

export const key = 'scan';
