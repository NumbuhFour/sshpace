
import Command from '../extendable_command';
import Universe from '../../server/universe.js';

export default class ListPlayers extends Command {

  handleInput(line, params, pipe) {
    const players = Universe.getOnlinePlayers();
    this.client.log("Currently " + players.length + " players online.\n");
    for(let p in players) {
      this.client.log(players[p]);
    }
    this.resolve(0, JSON.stringify(players));
  }
}

export const key = 'lsplayers';
