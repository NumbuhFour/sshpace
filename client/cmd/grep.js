
import Command from '../extendable_command';

export default class Status extends Command {

  handleInput(line, params, pipe) {
    if (!pipe) this.resolve(0);
    else if (pipe instanceof Array) {
      const search = params.slice(1).join(' ');
      console.log( 'Searching for "' + search + '" in ' + JSON.stringify(pipe));
      this.resolve(0, pipe.filter(o => {
        return JSON.stringify(o).indexOf(search) != -1; 
      }));
    }
    else {
      const search = params.slice(1).join(' ');
      this.resolve(0, (pipe.toString().indexOf(search) == -1) ? undefined: pipe);
    }
  }
}

export const key = 'grep';
