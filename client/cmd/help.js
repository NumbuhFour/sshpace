
import Command from '../extendable_command';

export default class Help extends Command {

  handleInput(line, params, pipe) {
    let cmds = [];
    let cmdHan;
    for (var i in  this.commandHandlerStack) {
      cmdHan = this.commandHandlerStack[i];
      cmds = cmds.concat(Object.keys(cmdHan.getCommandList()));
    }
    
    this.resolve(0, cmds);
  }
}

export const key = 'help';
