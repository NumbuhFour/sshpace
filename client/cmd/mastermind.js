
import Command from '../extendable_command';

const _colors = {
  'R': '{red-bg}{black-fg}R{/}',
  'G': '{green-bg}{black-fg}G{/}',
  'B': '{blue-bg}{white-fg}B{/}',
  'C': '{cyan-bg}{black-fg}C{/}',
  'Y': '{yellow-bg}{black-fg}Y{/}',
  'M': '{magenta-bg}{black-fg}M{/}',
  'W': '{white-bg}{black-fg}W{/}',
};

export default class Look extends Command {

  init() {
    this.running = false;
    this.doubles = true;
  }

  handleInput(line, params, pipe) {
    if(!this.running) {
      this.slotsCount = parseInt(params[1] || 4);
      this.colorsCount = parseInt(params[2] || 4);

      
      if (this.slotsCount == NaN || this.colorsCount == NaN) {
        this.client.log('Usage: ' + this.key + ' [slots] [colors');
        this.resolve(0);
        return;
      }

      if(this.slotsCount > 8 || this.colorsCount > Object.keys(_colors).length) {
        this.client.log('Max slots: 8, Max colors: ' + Object.keys(_colors).length);
        this.resolve(0);
        return;
      }

      this.running = true;
      this.guesses = 0;
      this.client.terminal.pushPwd('Mastermind');
      
      // Make solution
      this.slots = [];
      const colorsTemp = Object.keys(_colors).slice(0, this.colorsCount);
      let r;
      for(let i = 0; i < this.slotsCount; i++) {
        r = Math.floor(Math.random() * colorsTemp.length);
        this.slots.push(colorsTemp[r]);
        if (!this.doubles) colorsTemp.splice(r, 1);
      }

      const msg = ["Welcome to Mastermind!",
                   "A code of length " + this.slotsCount + " has been made.",
                   "Your available colors are " + Object.values(_colors).slice(0, this.colorsCount).join(' '),
                   "Make your guess!",
                   "Enter 'exit' to quit."];
      msg.forEach(m => this.client.log(m));
    }

    else {
      if(line.trim().toLowerCase() == 'exit') {
        this.client.log("Goodbye!");
        this.client.terminal.popPwd();
        this.resolve(0);
        return;
      }

      // Validate guess
      let guess = line.replace(/[,|\s]+/g,'').toUpperCase(); // Remove spaces and commas

      if (guess.length !== this.slotsCount) {
        this.client.log('{red-fg}Incorrect number of pegs!{/} There are ' + this.slotsCount + ' slots!');
        return;
      }

      const colorLimit = Object.keys(_colors).slice(0, this.colorsCount);
      const slotsClone = this.slots.slice(0);
      let correctPos = 0, correctCol = 0;
      let remaining = '';
      // Check for correct position first
      for(let i = 0; i < guess.length; i++) {
        if(colorLimit.indexOf(guess[i]) == -1) {
          this.client.log('{red-fg}Invalid color option:{/} ' + guess[i]);
          return;
        }

        if(this.slots[i] == guess[i]){
          correctPos ++;
          slotsClone.splice(slotsClone.indexOf(guess[i]),1);
        }
        else {
          remaining += guess[i];
        }
      }

      // Now check for remaining correct colors
      for (let i = 0; i < remaining.length; i++){
        if(slotsClone.indexOf(remaining[i]) !== -1) {
          correctCol ++;
          slotsClone.splice(slotsClone.indexOf(remaining[i]), 1);
        }
      }


      this.guesses++;
      this.client.log('Guess #' + this.guesses + ': ' + guess.split('').map(c => _colors[c]).join(' '));
      this.client.log('Result: ' + correctPos + ' correctly positioned, ' + correctCol + ' correct colors.');
      if (correctPos == this.slots.length) {
        this.client.log('You win!');
        this.client.terminal.popPwd();
        this.resolve(0);
      }

    }
    
  }
}

export const key = 'mastermind';
