
import Command from '../extendable_command';

export default class Echo extends Command {

  handleInput(line, params, pipe) {
    let out = (typeof pipe === 'string' || pipe instanceof String) ? pipe : JSON.stringify(pipe) ;
    if (!out) out = params.slice(1).join(' ');
    this.resolve(0, out);
  }
}

export const key = 'echo';
