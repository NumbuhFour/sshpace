import ExtendableCommand from '../extendable_command';
import chatman from '../../server/chat_manager';

export default class Comms extends ExtendableCommand {

  handleInput(line, params, pipe) {
    if(this.feed) {
      if(line == 'cc') {
        this.client.log('Input grab done');
        this.resolve(0);
      }
      else
        chatman.sendMessage(this.client, this.client.channel.name, line); 
      return;
    }
    console.log("WAHT GOT COM COMMAND ", line);
    switch(params[1]) {
      case 'open':
        this.resolve(chatman.openComms(this.client, params[2]));
        break;
      case 'close':
        this.resolve(chatman.closeComms(this.client));
        break;
      case 'connect':
        this.resolve(chatman.enterChannel(this.client, params[2]));
        break;
      case 'disconnect':
        this.resolve(chatman.exitChannel(this.client));
        break;
      default:
        if(params.length == 1) {
          this.client.log('Input grab test')
          this.feed = true;
        } else {
          this.resolve(chatman.sendMessage(this.client, this.client.channel.name, params.slice(1).join(' ')));
        }
        break;
    }
  }
}

export const key = 'com';
