import Command from '../extendable_command';
import Drone from '../../server/models/drone';

export default class ListOwnedDrones extends Command {
  handleInput(line, params, pipe) {
    Drone.find({ _id : { $in : this.client.user.drones } }, (err, find) => {
      if (err) throw err;
      this.resolve(0, find.map( o => o.name ));
    });
  }
}

export const key = 'lsdrones'
