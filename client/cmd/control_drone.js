
import Command from '../extendable_command';
import DroneModel from '../../server/models/drone';
import CommandHandler from '../command_handler';
import { parsePipes, splitParams } from '../command_parse_util'; 
import Drone from '../drone';

export default class ControlDrone extends Command {

  init() {
    this.drone = undefined;
  }

  handleInput(line, params, pipe) {
    // Not remote'd in, first run
    if (this.drone == undefined) {
      const lookup = params.slice(1).join(' ');
      if (!lookup) {
        this.client.log('Usage: ' + key + ' (drone name)');

        return this.resolve(0);
      }

      DroneModel.find({ name : lookup, owner: this.client.user._id }, (err, find) => {
        if(err) throw err;
        if (find.length > 0) {
          this.client.log('Found drone: ' + find[0]);
          
          this.drone = new Drone(find[0]);
          
          this.comHan = new CommandHandler(__dirname + '/drone',
                                          this.client,
                                          this.closeSession.bind(this),
                                          [ this.drone ],
                                          this.parentCommandHandler);
          this.client.terminal.pushPwd('drone:' + this.drone.getName());

          this.client.log('Opening session...');
          this.client.terminal.addCmdLine('');
        }
        else {
          this.client.log("Drone not found.");
          this.resolve(0);
        }
      });

    }
    else {
      this.comHan.onSubmit(line);
    }
  }

  closeSession() {
    this.client.log('Closing session...');
    this.drone = undefined;
    this.client.terminal.popPwd();
    this.resolve(0);
    this.client.terminal.addCmdLine('');
  }

}

export const key = 'dronesh';
