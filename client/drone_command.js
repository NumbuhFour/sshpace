import ExtendableCommand from './extendable_command.js';

export default class DroneCommand extends ExtendableCommand {
  constructor (client, resolve, drone) {
    super(client, resolve);
    this.drone = drone;
  }
}

export const key = 'err';
